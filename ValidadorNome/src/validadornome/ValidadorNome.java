/*
 * Click https://www.hackerrank.com/challenges/valid-username-checker/problem?isFullScreen=true
 */
package validadornome;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
/**
 *
 * @author Eduardo T
 */
public class ValidadorNome {

    public static void main(String[] args) {       
        Pattern pattern ;
        Matcher matcher;
        String nome="[a-zA-Z][a-zA-Z][a-zA-Z][a-zA-Z][a-zA-Z][a-zA-Z][a-zA-Z][a-zA-Z]*";
        String  [] lista = new String [4];
        boolean [] valido = new boolean [4];
        String resul="";
        for (int i =0;lista.length>i;i++){
            int t= i+1;
            lista [i] =JOptionPane.showInputDialog(null,"Insira o " +t+ "º Nome");
        }
        for(int i=0;lista.length>i;i++){
            if(lista[i].length()<30){                
                pattern = Pattern.compile(nome);
                matcher = pattern.matcher(lista[i]);
                valido [i]=matcher.matches();
                matcher.find();
                if(valido[i]=true){
                    Pattern pattern1 = Pattern.compile(nome);
                    Matcher matcher1 = pattern1.matcher(lista[i]);
                    valido [i]=matcher1.matches();
                }
            }else {
               valido [i]=false; 
            }
            
            resul= resul + "\n"+ "O Nome "+ lista[i]+" é "+ valido[i];
        } 
        JOptionPane.showMessageDialog(null,resul );    
    }
    
}
