/*
 * Click https://www.hackerrank.com/challenges/java-regex/problem?isFullScreen=true
 */
package javaregex;/**
 *
 * @author Eduardo T
 */
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;   
public class JavaRegex {   
    public static void main(String[] args) {        
       
        String ip="^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."+
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."+
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."+
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
        String  [] lista = new String [3];
        boolean [] valido = new boolean [3];
        String resul="";

        for (int i =0;lista.length>i;i++){
            int t= i+1;
            lista [i] =JOptionPane.showInputDialog(null,"Insira o "+t+"º IP a ser testado");
        }
        for(int i=0, x =0;lista.length>i;i++,x++){
            Pattern pattern = Pattern.compile(ip);
            Matcher matcher = pattern.matcher(lista[i]);
            valido [i]=matcher.matches();
            matcher.find();
            resul= resul + "\n"+ "O IP "+ lista[i]+" é "+ valido[i];
        } 
        JOptionPane.showMessageDialog(null,resul);           
    }  
}
