/*
 * Click https://www.hackerrank.com/challenges/java-datatypes/problem?isFullScreen=true
 */
package variaveisprimitivas;
import java.math.BigInteger;
import javax.swing.JOptionPane;
/**
 *
 * @author Eduardo T
 */
public class VariaveisPrimitivas {
    
    public static void main(String[] args) {
        byte b = 8; //-128 a +127
        short s = 16; //-32768 a 32767
        int i = 32; // -2.147.483.648 a 2.147.483.647
        long l =64; //-9.223.372.036.854.775.808 a 9.223.372.036.854.775.807
        int x;
        String temp;        
        temp = JOptionPane.showInputDialog("Insira um valor a ser testado");
        BigInteger aux = new BigInteger(temp);
        x = aux.bitLength();
        if(x<b){
           JOptionPane.showMessageDialog(null,"O valor " + aux +"\n"+ "pode ser armazenado nas Variaveis primitivas: \n"+
                   "byte \n"+"short \n"+"int \n"+"long \n"); 
        }else if(x<s){
            JOptionPane.showMessageDialog(null,"O valor " + aux +"\n"+ "pode ser armazenado nas Variaveis primitivas: \n"+
                   "short \n"+"int \n"+"long \n");
        }else if (x<i){
            JOptionPane.showMessageDialog(null,"O valor " + aux +"\n"+ "pode ser armazenado nas Variaveis primitivas: \n"+
                   "int \n"+"long \n");
        }else if(x<l){
            JOptionPane.showMessageDialog(null,"O valor " + aux +"\n"+ "pode ser armazenado somente na Variavel: \n"+"long \n");
        }else {
            JOptionPane.showMessageDialog(null,"O valor '" + aux +"'\n"+ "Não pode ser armazenado em nenhuma da variaveis Primitivas");
        }  
        
    }

}    
 
