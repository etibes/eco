/*
 * Click https://www.hackerrank.com/challenges/simple-array-sum/problem?isFullScreen=true
 */
import javax.swing.JOptionPane;

/**
 *
 * @author Eduardo T
 */



public class SomaArray {

   
    public static void main(String[] args) {
        int [] lista = {1,2,3,4,5,25,6,4,8};
        int soma = 0;
        
        for (int i =0; i < lista.length; i++){
            soma = soma+lista[i];
        }
    
       JOptionPane.showMessageDialog(null,"A SOMA DO ARRAY É = "+ soma );
        
        
    }
}