/*
 * Click https://www.hackerrank.com/challenges/repeated-string/problem?isFullScreen=true
 */
package repeatedstring;
import javax.swing.JOptionPane;
/**
 *
 * @author Eduardo T
 */
public class RepeatedString {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String s ;//grupo de caracteres a serem repetidos
        char temp ;
        int n ;// numero de maximo de repetições/caracteres 
        int c ;
        int resul=0; // numero de vezes que "a" foi repetida no grupo de caracteres
        do{
        s = JOptionPane.showInputDialog( "Informne o grupo de caractertes a serem repetidos");
        c = s.length();
        } while(c<=0); 
        do{    
        n = Integer.parseInt( JOptionPane.showInputDialog( "Informne numero de vezes que os caracteres do grupo serão repetidos"));        
        }while (n<1 && n>10000);
        while(c < n){      
            s=s+s;
            c = s.length();
        }        
        s = s.substring(0,n);
        for (int i =0;i<s.length();i++){
            temp = s.charAt(i);
            if(temp == 'a' || temp == 'A'){
              resul = resul+1;  
            }
        }        
        JOptionPane.showMessageDialog(null,"Grupo de caracteres infinitos '" + s.toUpperCase()+"'") ;
        JOptionPane.showMessageDialog(null, "Quantidade de vezes que a letra 'A' aparece no grupo é = " +resul );
    }
    
}
