/*
 * Click https://www.hackerrank.com/challenges/java-strings-introduction/problem?isFullScreen=true
 */
package manutencaostrings;

import javax.swing.JOptionPane;

/**
 *
 * @author Eduardo T
 */
public class ManutencaoStrings {
    public static void main(String[] args) {
       
        String a,b,c="";
        char asp ='"';
        int aux,aux2;
        a = JOptionPane.showInputDialog(null,"Insira a primeira frase ou palavra:");
        b = JOptionPane.showInputDialog(null,"Insira a segunda frase ou palavra:");
        aux=a.compareTo(b);
        if (aux>0){
            c ="SIM";
        }else{
            c="NAO";
        }
        aux2 = a.length()+b.length();
        JOptionPane.showMessageDialog(null, "A soma dos comprimentos das variaveis informadas é " +asp+aux2+asp+ "\n" + 
                "A primeira palavra é lexicograficamente maior do que a segunda?" + "\n"+ c+ "\n Junção das palavras é "+asp + a+ " "+b+asp);
    }
    
}
