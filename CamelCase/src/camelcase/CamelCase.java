/*
 * Click https://www.hackerrank.com/challenges/camelcase/problem?isFullScreen=true
 */
package camelcase;
import javax.swing.JOptionPane;
/**
 *
 * @author Eduardo T
 */
public class CamelCase {
    public static void main(String[] args) {
    String frase= JOptionPane.showInputDialog("Insira a frase para realizar a contagem");
    String [] words = frase.split("[a-z][A-Z]");
    int cont=words.length ;
    //LowerCamelCase
    JOptionPane.showMessageDialog(null,"O numero de palavras encontradas é =\n "+cont);        
    }
}
