/*
 * Click https://www.hackerrank.com/challenges/java-if-else/problem?isFullScreen=true
 */
package condicinais;
import javax.swing.JOptionPane;
/**
 *
If  is odd, print Weird
If  is even and in the inclusive range of 2 to 5, print Not Weird
If  is even and in the inclusive range of 6 to 20 , print Weird
If  is even and greater than 20, print Not Weird
Se for estranho, imprima Estranho
Se for par e estiver no intervalo inclusivo de 2 a 5, imprima Not Weird
Se for par e estiver no intervalo inclusivo de 6 a 20 , imprima Estranho
Se for par e maior que 20, imprima Not Weird
* 
 * @author Eduardo T
 */
public class Condicinais {
    public static void main(String[] args) {
        int n;
        
        do{
        n = Integer.parseInt( JOptionPane.showInputDialog( "Informe um numero Inteiro e positivo"));
        }while (n<=1 && n>=100);
        if (n%2==0){
            if(n>=2 && n<=5){
                JOptionPane.showMessageDialog(null, "O numero "+ n+ " é "+"Not Weird");
            }else if(n>=6 && n<=20){
                JOptionPane.showMessageDialog(null,"O numero "+ n+ " é "+ "Weird");
            }else if(n>20){
                JOptionPane.showMessageDialog(null,"O numero "+ n+ " é "+ "Not Weird");
            }
        }else {
            JOptionPane.showMessageDialog(null,"O numero "+ n+ " é "+"Weird");
        }
      
    }
    
}
